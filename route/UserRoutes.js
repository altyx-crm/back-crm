const UserController = require('../controller/UserController');
const { createUser } = require('../schema/UserSchema');

module.exports = (fastify, opts, next) => {
  fastify.post('/user', { schema: createUser }, async (req, res) => {
    try {
      const response = await UserController.createUser(req, res, fastify);
      res.code(response.statusCode).send({ ...response.data });
    } catch (error) {
      throw new Error(error);
    }
  });

  /* fastify.post('/user', (req, res) => {
    UserController.signup(req, res, next);
  }); */
  next();
};
