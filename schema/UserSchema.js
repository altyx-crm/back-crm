const createUser = {
  body: {
    type: 'object',
    properties: {
      login: { type: 'string' },
      email: { type: 'string' },
      passsword: { type: 'string' },
    },
    required: ['email', 'login', 'password'],
  },
  response: {
    201: {
      type: 'object',
      properties: {
        login: { type: 'string' },
        email: { type: 'string' },
        oid: { type: 'number' },
        user_id: { type: 'number' },
      },
      required: [],
    },
  },
};

module.exports = { createUser };
