const fastify = require('fastify')({
  logger: true,
});

fastify.use(require('cors')());
fastify.register(require('./route/UserRoutes'));
fastify.register(require('fastify-postgres'), {
  connectionString: 'fake://fake:fake@fake:fake/fake',
});

const start = async () => {
  try {
    await fastify.listen(8080);
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
