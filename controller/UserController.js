const UserService = require('../services/UserService');

exports.createUser = async (req, reply, fastify) => {
  try {
    const User = new UserService(fastify);
    const row = await User.createUser(req.body);
    return {
      test: 1,
      statusCode: 201,
      data: row[0],
    };
  } catch (err) {
    return err;
  }
};
