const argon2 = require('argon2');

class UserService {
  constructor(fastify) {
    this.fastify = fastify;
  }

  async createUser(user) {
    const login = await argon2.hash(user.login);
    const email = await argon2.hash(user.email);
    const password = await argon2.hash(user.password);
    const client = await this.fastify.pg.connect();
    const { rows } = await client.query(
      'INSERT INTO Users(login, email, password) values($1, $2, $3) RETURNING oid, user_id, login, email', [login, email, password],
    );
    client.release();
    return rows;
  }
}

module.exports = UserService;
